#!/usr/bin/env python

from os import environ
from json import dumps
from operator import itemgetter
from constructs import Construct
from cdktf import App, TerraformStack
from imports.aws import AwsProvider, AwsProviderDefaultTags
from imports.aws.s3 import S3Bucket, S3BucketPublicAccessBlock, S3BucketPolicy
from imports.aws.apigateway import ApiGatewayRestApi
from imports.aws.cloudfront import *

class FlattenStack(TerraformStack):
    def __init__(self, scope: Construct, ns: str, config: dict):
        super().__init__(scope, ns)

        env, region = itemgetter('environment', 'region')(config)

        provider = AwsProvider(self, 'Aws',
                region=region,
                default_tags=AwsProviderDefaultTags(
                    tags={
                        'ProjectId': 'flatten',
                        'Environment': env }))

        hosting = S3Bucket(self, 'Hosting',
                bucket=f'flatten-hosting-{env}',
                acl='private')

        S3BucketPublicAccessBlock(self, 'HostingPermissions',
                bucket=hosting.id,
                block_public_acls=True,
                block_public_policy=True,
                ignore_public_acls=True,
                restrict_public_buckets=True)

        oai = CloudfrontOriginAccessIdentity(self, 'Oai',
                comment = "OAI to access flatten frontend bucket")

        policy = {
              'Version': '2012-10-17',
              'Statement': {
                  'Effect': 'Allow',
                  'Action': 's3:GetObject',
                  'Resource': f'{hosting.arn}/*',
                  'Principal': {
                      'AWS': [oai.iam_arn]}}}

        S3BucketPolicy(self, 'HostingPolicy',
                bucket=hosting.id,
                policy=dumps(policy))

        gateway = ApiGatewayRestApi(self, 'Gateway',
                name="Flatten Rest API")

        hosting_origin = CloudfrontDistributionOrigin(
                domain_name=hosting.bucket_regional_domain_name,
                origin_id='S3Origin',
                s3_origin_config=CloudfrontDistributionOriginS3OriginConfig(
                    origin_access_identity=oai.cloudfront_access_identity_path ))

        hosting_cache_behavior = CloudfrontDistributionDefaultCacheBehavior(
                allowed_methods=['GET','HEAD'],
                cached_methods=['GET','HEAD'],
                target_origin_id='S3Origin',
                forwarded_values=CloudfrontDistributionDefaultCacheBehaviorForwardedValues(
                    query_string=True, # bug
                    cookies=CloudfrontDistributionDefaultCacheBehaviorForwardedValuesCookies(
                        forward='none')),
                viewer_protocol_policy='redirect-to-https')

        api_origin = CloudfrontDistributionOrigin(
                domain_name=f'{gateway.id}.execute-api.{region}.amazonaws.com',
                origin_path=f'/{env}',
                origin_id='APIGWOrigin',
                custom_origin_config=CloudfrontDistributionOriginCustomOriginConfig(
                    http_port=80,
                    https_port=443,
                    origin_protocol_policy='https-only',
                    origin_ssl_protocols=['TLSv1.2'] ))

        api_cache_behavior = CloudfrontDistributionOrderedCacheBehavior(
                path_pattern='/api/*',
                allowed_methods=['DELETE','GET','HEAD','OPTIONS','PATCH','POST','PUT'],
                cached_methods=['GET','HEAD'],
                target_origin_id='APIGWOrigin',
                forwarded_values=CloudfrontDistributionOrderedCacheBehaviorForwardedValues(
                    query_string=True,
                    headers=['Origin'],
                    cookies=CloudfrontDistributionOrderedCacheBehaviorForwardedValuesCookies(
                        forward='none')),
                viewer_protocol_policy='redirect-to-https')

        CloudfrontDistribution(self, 'Distribution',
                comment = f'Flatten {env} environment',
                enabled = True,
                is_ipv6_enabled = True,
                default_root_object = 'index.html',
                origin=[hosting_origin, api_origin],
                default_cache_behavior=hosting_cache_behavior,
                ordered_cache_behavior=[api_cache_behavior],
                restrictions=CloudfrontDistributionRestrictions(
                    geo_restriction=CloudfrontDistributionRestrictionsGeoRestriction(
                        restriction_type='none' )),
                viewer_certificate=CloudfrontDistributionViewerCertificate(
                    cloudfront_default_certificate=True ))

app = App()
FlattenStack(app, 'flatten-dev', {
    'environment': 'dev',
    'region': environ.get('AWS_DEFAULT_REGION') })
FlattenStack(app, 'flatten-staging', {
    'environment': 'staging',
    'region': environ.get('AWS_DEFAULT_REGION') })
FlattenStack(app, 'flatten-production-eu', {
    'environment': 'production',
    'region': 'eu-central-1' })
FlattenStack(app, 'flatten-production-us', {
    'environment': 'production',
    'region': 'eu-east-1' })
app.synth()
