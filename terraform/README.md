# Exercise # 2

## Problem statement

Write a Terraform configuration that will create a production ready
infrastructure needed for publishing a high scale microservice to the internet.
Write the configuration for the cloud provider you're more familiar with.

## Overview

Terraform supports many clouds and infrastructure platforms. The problem openly
asks for the choice of a cloud provider. Adopting a provider rather than
another in the cloud segment has reasoning that does not apply to the context
of this exercise (i.e. company policies, partnerships, investments in specific
technologies). Anyway, the request explicitly asks for the one I'm most
familiar with, so it will be AWS.

As for the first exercise, to stand out but also to try something new, I
decided to use the Terraform CDK rather than the well-known HCL. In my
experience, especially with the AWS CDK, I have seen how much the use of a
"common" language rather than a DSL has enormous advantages:

- Widespread knowledge, therefore applicability even for those who do not know
  HCL directly;
- Enhancement with typical constructs of programming languages (i.e. loading
  the configuration from a file);
- Organize files more effectively thanks to the OOO.

Among the many versions, I decided to use Python for the speed of the realization.

The system is a web application for flattening arrays (as in the previous
exercise). All microservices will adopt serverless technologies to achieve high
scale, availability and resiliency.

### System architecture

``` plantuml
left to right direction

node Frontend as FE
node Backend as BE
database Data as DB

FE ..> () HTTP : use
HTTP - BE
BE --> DB
```

The infrastructure must be production-ready, based on the
[AWS Well-Architected Framework](https://docs.aws.amazon.com/wellarchitected/latest/framework/welcome.html)
I will make sure that the infrastructure has well-defined characteristics.

### Operational excellence

#### Observability

Application logs will be collected by CloudWatch and globally accessible with CloudWatch ServiceLens. CloudWatch will exhibit the RED dashboard.

Metadata and performance will be tracked with AWS X-Ray, always aggregated by CloudWatch.

AWS CloudTrail for governance, compliance and auditing logs.

KPIs defined.

#### Rotating logs

The life cycle of the logs will be as follows:
- The first 14 days, the logs will be accessible and searchable on CloudWatch;
- The first 6 months, the logs will be accessible via S3 and searchable with Amazon Athena;
- From 6 to 12 months, the logs will not be accessible unless explicitly requested and stored on S3 Glacier.

#### Versioning

The application code will be versioned for microservices in various repositories on CodeCommit.

The infrastructure code will be versioned in the same way.

#### Continuous integration

Builds and tests will be performed on each commit of the production branch, thanks to CodeBuild and CodePipeline.

The process for infrastructure code will be equivalent. Pipelines are self-maintaining pipelines.

#### Continuous deployment

Each tagged application version will be releasable from GUI.

The infrastructure can be destroyed, rebuilt or replicated with minimal effort thanks to environment variables and parameters.

### Security

#### Secrets not versioned

Repositories contain no secrets. Access is governed by AWS IAM and AWS STS roles.

#### SAST

Pipelines have a static code analysis step for security and formatting.

Dependencies are also scanned for a security check.

#### Networking

VPCs and subnets are isolated from other applications and configured to ensure confidentiality: only public resources are exposed.

#### Encryption in transit and at rest

The infrastructure must support encryption in transit and at rest.

### Reliability

#### Metrics and notifications

Thanks to CloudWatch and SNS, metrics and sentinels will be set to react to specific events such as failure scenarios, budget etc.

#### Data

Thanks to the features of DynamoDB, automatic backups and policies for their governance will be set.

### Cost optimization

#### Plan and reports

An iterative plan is defined for TCO monitoring and subsequent optimization.

## Solution

Some of the points I listed are not exclusive to the infrastructure layer.
Others would take much time for a single exercise. Therefore I have chosen to
describe some concepts in this document rather than Terraforming services.
Furthermore, the nature of serverless technology allows me to imply several
notions (i.e. high availability).

``` plantuml
skinparam shadowing false

!define AWSPuml https://raw.githubusercontent.com/awslabs/aws-icons-for-plantuml/v11.1/dist
!includeurl AWSPuml/AWSCommon.puml
!includeurl AWSPuml/SecurityIdentityCompliance/CertificateManager.puml
!includeurl AWSPuml/NetworkingContentDelivery/Route53.puml
!includeurl AWSPuml/NetworkingContentDelivery/CloudFront.puml
!includeurl AWSPuml/Compute/Lambda.puml
!includeurl AWSPuml/Storage/SimpleStorageService.puml
!includeurl AWSPuml/ApplicationIntegration/APIGateway.puml
!includeurl AWSPuml/Database/DynamoDB.puml
!includeurl AWSPuml/DeveloperTools/XRay.puml
!includeurl AWSPuml/ManagementGovernance/CloudWatch.puml
!includeurl AWSPuml/ManagementGovernance/CloudTrail.puml
!includeurl AWSPuml/Storage/SimpleStorageServiceGlacier.puml
!includeurl AWSPuml/Analytics/Athena.puml
!includeurl AWSPuml/DeveloperTools/CodeCommit.puml
!includeurl AWSPuml/DeveloperTools/CodePipeline.puml
!includeurl AWSPuml/DeveloperTools/CodeBuild.puml
!includeurl AWSPuml/SecurityIdentityCompliance/Cognito.puml
!includeurl AWSPuml/SecurityIdentityCompliance/IdentityandAccessManagement.puml
!includeurl AWSPuml/SecurityIdentityCompliance/IdentityAccessManagementAWSSTS.puml

actor UserAgent as agent
Cognito(upool, Users, UserPool)
Cognito(ipool, Identities, IdentityPool)
IdentityAccessManagementAWSSTS(sts, STS, IAM token)

CertificateManager(acm, CA, Certificate)
Route53(dns, DNS, Record)
CloudFront(cdn, CDN, Distribution)
Lambda(edge, Proxy, Lambda@Edge)
SimpleStorageService(bucket, Source, Bucket)
IdentityandAccessManagement(iam, Authorizer, IAM)

rectangle VPC #FFF;line.dashed {
  APIGateway(api, /flatten, POST)
  Lambda(engine, Flatten, Alias)
  DynamoDB(db, History, Table)
}

rectangle Logs #FFF;line.dashed {
  XRay(xray, Performance, Metrics)
  CloudWatch(log, Logs collector, ServiceLens)
  SimpleStorageService(logstore, Log store, Over 14 days)
  Athena(analytics, Log analysis, Query service)
  SimpleStorageServiceGlacier(longlogstore, Archive, Over 6 months)
  CloudTrail(trail, Governance, Compliance and risk auditing)
}

rectangle DevOps #FFF;line.dashed {
  CodeCommit(repo, VCS, Codebase)
  CodeBuild(build, Builder, Create artificats)
  CodePipeline(pipeline, Pipeline, CI/CD orchestrator)
}

agent -right-> cdn
agent --> upool
agent --> ipool
ipool --> sts

cdn .up.> dns
cdn .up.> acm
cdn -down-> edge
cdn -down-> bucket : OAI
cdn -right-> api : HTTPS
api -right-> engine : AWS Proxy
api -down-> iam : AWS_IAM
engine -right-> db
engine .down.> xray : use

log --> logstore : export task
analytics -right-> logstore
logstore --> longlogstore : lifecycle rule

repo <--> pipeline
pipeline --> build
```

## Note

- All diagrams in this page use [PlantUML](https://plantuml.com/) that is a
  Diagram as Code technology.
