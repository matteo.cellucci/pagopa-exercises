# Exercises list

- [Flatten array](flatten)
- [Terraform infrastructure](terraform)

## Note

- For commit messages, I'll use the [AngularJS convention](https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y) and the [best practices of cbeams](https://cbea.ms/git-commit/).
