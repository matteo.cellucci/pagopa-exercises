#include <stdio.h>
#include <stdlib.h>

#include "includes/error.h"

void
syntax_error(char current, char *expected)
{
  printf("Synatx error: expecting '%s' instead of '%c'\n", expected, current);
  exit(EXIT_FAILURE);
}
