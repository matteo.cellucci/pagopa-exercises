#include <stdio.h>
#include <stdlib.h>

#include "includes/engine.h"

void
print_flatten(int *array, size_t size)
{
  printf("[");
  for (int i = 0; i < size; i++)
  {
    if (i == size - 1)
    {
      printf("%d", array[i]);
    }
    else
    {
      printf("%d,", array[i]);
    }
  }
  printf("]\n");
}

void
flatten_recursive(ary *root, int **array, size_t *size)
{
  if (root->size == 0)
  {
    *array = (int*) realloc(*array, ++(*size) * sizeof(int));
    *(*array + *size - 1) = *((int*) root->value);
  }
  else
  {
    for (int i = 0; i < root->size; i++)
    {
      flatten_recursive((ary*) root->value + i, array, size);
    }
  }
}

int*
flatten(ary *root)
{
  int *result = NULL;
  size_t size = 0;
  flatten_recursive(root, &result, &size);
  print_flatten(result, size);

  return result;
}
