#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "includes/controller.h"
#include "includes/error.h"

int
peek(char **list, char expected)
{
  int result = 0;
  if (**list == expected)
  {
    result = 1;
  }

  return result;
}

void
match(char **list, char expected)
{
  if (!peek(list, expected))
  {
    syntax_error(**list, &expected);
  }
  (*list)++;
}

void
free_arys(ary *root)
{
  if (root->size != 0)
  {
    for (int i = 0; i < root->size; i++)
    {
      free_arys((ary*) root->value + i);
    }
  }
  free(root->value);
}

void
digit(char **list, ary *element, int modifier)
{
  if (**list >= '0' && **list <= '9')
  {
    int current_value = **list - '0';
    if (modifier < 0)
    {
      *((int*) element->value) -= current_value;
    }
    else
    {
      *((int*) element->value) += current_value;
    }
    match(list, **list);
  }
  else
  {
    syntax_error(**list, "a digit");
  }
}

void
digit_excluding_zero(char **list, ary *element, int modifier)
{
  if (**list == '0')
  {
    syntax_error('0', "a non zero digit");
  }
  digit(list, element, modifier);
}

void
integer(char **list, ary *element)
{
  if (peek(list, '0'))
  {
    element->value = realloc(element->value, sizeof(int));
    *((int*) element->value) = **list - '0';
    match(list, '0');
  }
  else
  {
    int modifier = 1;
    if (peek(list, '-'))
    {
      modifier *= -1;
      match(list, '-');
    }
    else if (peek(list, '+'))
    {
      match(list, '+');
    }

    element->value = realloc(element->value, sizeof(int));
    *((int*) element->value) = 0;
    digit_excluding_zero(list, element, modifier);

    while (peek(list, '0') || peek(list, '1') || peek(list, '2') || peek(list, '3') ||
           peek(list, '4') || peek(list, '5') || peek(list, '6') || peek(list, '7') ||
           peek(list, '8') || peek(list, '9'))
    {
      *((int*) element->value) *= 10;
      digit(list, element, modifier);
    }
  }
}

void
element(char **list, ary *element)
{
  element->size = 0;
  element->value = NULL;

  if (peek(list, '['))
  {
    array(list, element);
  }
  else
  {
    integer(list, element);
  }
}

void
array(char **list, ary *parent)
{
  match(list, '[');
  parent->value = realloc(parent->value, ++parent->size * sizeof(ary));
  element(list, (ary*) parent->value + parent->size - 1);
  while (peek(list, ','))
  {
    match(list, ',');
    parent->value = realloc(parent->value, ++parent->size * sizeof(ary));
    element(list, (ary*) parent->value + parent->size - 1);
  }
  match(list, ']');
}

ary*
parse(char *list)
{
  ary *root = malloc(sizeof(ary));
  root->size = 0;
  root->value = calloc(root->size, sizeof(ary));
  array(&list, root);

  return root;
}
