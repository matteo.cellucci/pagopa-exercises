#ifndef ENGINE_H_
#define ENGINE_H_

#include "controller.h"

/**
 * Utility to print the flattened array
 */
void print_flatten(int*, size_t);

/**
 * Utility to recursively flatten an array (ary)
 */
void flatten_recursive(ary*, int**, size_t*);

/**
 * Interface to flatten a given array (ary)
 */
int* flatten(ary*);

#endif
