#ifndef CONTROLLER_H_
#define CONTROLLER_H_

/**
 * An ary is an array or an integer
 */
typedef struct ary {
  size_t size;
  void *value;
} ary;

/**
 * Look the current token and compare with an expected value
 */
int peek(char**, char);

/**
 * Peek current token and exit if not valid
 */
void match(char**, char);

/**
 * Free the memory from an entire ary
 */
void free_arys(ary*);

/**
 * EBNF rule, see README for more
 */
void digit(char**, ary*, int);

/**
 * EBNF rule, see README for more
 */
void digit_excluding_zero(char**, ary*, int);

/**
 * EBNF rule, see README for more
 */
void integer(char**, ary*);

/**
 * EBNF rule, see README for more
 */
void element(char**, ary*);

/**
 * EBNF rule, see README for more
 */
void array(char**, ary*);

/**
 * Start lexing and parsing the input string
 */
ary* parse(char*);

#endif
