#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "includes/controller.h"
#include "includes/engine.h"

void
usage()
{
  printf("Usage: flatten [-h] ARRAY\n");
  printf("\tFlatten given array.\n\n");
  printf("\tOptions:\n");
  printf("\t  -h\tShow this message\n");
}

int
main(int argc, char** argv)
{
  int option = -1;

  if (argc == 1)
  {
    usage();
    exit(EXIT_FAILURE);
  }

  while ((option = getopt(argc, argv, "h")) != -1)
  {
    switch (option)
    {
      case 'h':
        usage();
        return 0;
      default:
        usage();
        exit(EXIT_FAILURE);
    }
  }

  ary *root = parse(argv[argc - 1]);
  int *result = flatten(root);

  free_arys(root);
  free(root);
  free(result);

  return 0;
}
