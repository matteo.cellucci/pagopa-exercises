# Exercise # 1

## Problem statement

Write some code that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers. e.g. [[1,2, [3]], 4] -> [1,2,3,4].

## Overview

I firmly believe that the best technology does not exist. If we use this term, we must contextualize it with a use case to identify the best technology for the problem and, sometimes, even this is not enough. Technological choice can also be influenced by external factors and not only endogenous.

The exercise, only apparently trivial, hides the biggest problem of choosing the language for its resolution as it is not specified by the trace.

A first-choice could fall on Ruby, which turns out to win for speed of realization with a native method of the language:

``` ruby
array_to_flatten.flatten
```

Equally JavaScript:

``` javascript
arrayToFlatten.flat(Infinity)
```

Given the nature of the problem, which is an exercise for a selection process, these solutions are perhaps the least suitable as they lack deeper reasoning and deductions.

To facilitate the revision of the exercise, one could consider adopting a language [used on a large scale](https://pypl.github.io/PYPL.html) such as Java or Python. I could highlight the difference in the various available solutions in this case.

For example, I could evaluate a recursive solution in Java:

``` java
public static Integer[] flatten(Object[] array) {
  List<Integer> result = new ArrayList<Integer>();
  flattenToList(array, result);
  return result.toArray(new Integer[result.size()]);
}

private static void flattenToList(Object[] array, List<Integer> list) {
  for (Object element: array) {
    if (element instanceof Integer) {
      list.add((Integer) element);
    } else {
      flattenToList((Object []) element, list);
    }
  }
}
```

or an iterative solution in Python:

``` python
def flatten(array):
    while any (isinstance(e, list) for e in array):
        i = 0
        while i < len(array):
            if isinstance(array[i], list):
                array[i: i + len(array[i])] = array[i]
            i + = 1
    return array
```

However, flattening an array is not an algorithmic problem.

In both languages, several solutions with complexity `O(n)` can be found, with `n` the number of total integers (we could see the array as an n-ary tree, then the number of leaves). It is interesting to note how the spatial complexity is inverse to that computation. In the iterative solution, we will have `O(n)` against `O(n + m)` for the recursive, with `m` the number of nested arrays (with a tree n-ary the sum is the number of nodes that compose it).

These arguments, however, elude the agility of the modern world, increasingly business-centric: dwelling on this level of detail without a corresponding product gain is not an effective method.

Therefore, the choice has fallen on the raw C as it makes the problem even more exciting and a fair challenge between technology and logic. It also distinguishes me from any other candidates in the selection process.

One more opportunity to use an essential language for our area. IT is merging into all industries, influencing and being influenced by them: C is considered one of the most eco-friendly languages, given its low energy consumption, it reduces the [environmental impact](https://hackaday.com/2021/11/18/c-is-the-greenest-programming-language/) of this exercise :)

### Lateral Thinking: Bonus

Since the problem doesn't specify how the array is given, I can't help highlight a fun workaround that takes advantage of lateral thinking:

``` zsh
$ echo "[[1,2,[3]],4]" | sed -e "s/[^0-9,-\+]//g" -e "s/\(.*\)/\[\1\]/g"
```

## Solution

The idea is to proceed with small increments, and each one of them will be tagged by a VCS tag.

### Increments

1. Input management: create a lexer and a parser to identify the input string as an array;
1. Flattening at depth 1: create the interpreter to flat the array counting a single depth level (ie [[1,2,[3]],4] -> [1,2,[3],4]);
1. Total flattening: extend the interpreter to flatten all depth levels;
1. Management of errors and specific corner cases.

#### f1.0.0

I'll use following EBNF grammar:

```
array = "[", element, { "," , element }, "]" ;
element = integer | array ;
integer = "0" | ( [ "-" | "+" ], digit_excluding_zero, { digit } ) ;
digit = "0" | digit_excluding_zero ;
digit_excluding_zero = "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" ;
```

#### f2.0.0 and f2.1.0

They consist on just one increment, it would be more difficult handle them as planned.

#### f3.2.0

There is an open point:

- Although modern OS manage memory effectively, it is considered good (and safe) to clean the memory before exiting. When handling errors with `exit`, it would be good to free up the memory first.

## How to run it

You can just move inside project folder and type `bin/flatten` to read the usage.

You can also make your own build with `make build`.
